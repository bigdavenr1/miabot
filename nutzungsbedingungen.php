<?php
error_reporting(E_ALL ^ E_NOTICE);
session_start();
?>
<!DOCTYPE html>
<html lang="de">
<head>
    <title>Miabot.de</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

</head>

<body>
<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '125564491479635',
            xfbml      : true,
            version    : 'v2.12'
        });

        FB.AppEvents.logPageView();
    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<!-- Facebook Pixel Code -->
<script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window,document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '170965336587525');
    fbq('track', 'PageView');
    fbq('track', 'ViewContent');
</script>
<noscript>
    <img height="1" width="1" src="https://www.facebook.com/tr?id=170965336587525&ev=PageView&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->
<header>
    <a href="start_page.php">
        <img src="icons_mobil/miabot_ueberschrift_mobile.svg"/>
        <img src="miabot_ueberschrift+logo_desktop.svg"/>
    </a>
</header>

<input type="checkbox" id="navchanger">
<nav class="<?php echo !empty($_SESSION['user']) ? 'eingeloggt' : 'ausgeloggt';?>">
    <ul id="menu_top">
        <li class="<?php echo !empty($_SESSION['user']) ? 'eingeloggt' : 'ausgeloggt';?>"> <a href="new_message.php">
                <img src="iconsimg/neue_fp.svg"/> NEUE FLASCHENPOST
            </a></li>
        <li class="<?php echo !empty($_SESSION['user']) ? 'eingeloggt' : 'ausgeloggt';?>"> <a href="my_messages.php">
                <img src="iconsimg/pfeil_eigene_fp.svg"/> EIGENE FLASCHENPOST
            </a></li>
        <li class="<?php echo !empty($_SESSION['user']) ? 'eingeloggt' : 'ausgeloggt';?>"> <a href="received_message.php">
                <img src="iconsimg/pfeil_erhaltene_fp.svg"/> GEFUNDENE FLASCHENPOST
            </a></li>
    </ul>

    <ul id="menu_bottom">
        <li class="fb-logout <?php echo !empty($_SESSION['user']) ? 'eingeloggt' : 'ausgeloggt';?>"> <a href="index.php?action=logout">
                <img src="iconsimg/fb-art_sml.png"/> LOGOUT
            </a></li>
        <li class="impressum-links"> <a href="impressum.php"><img src="icons_mobil/mobile_impressum.svg"/> IMPRESSUM</a></li>
        <li class="impressum-links"> <a href="datenschutz.php"><img src="icons_mobil/mobile_impressum.svg"/> DATENSCHUTZ</a></li>
        <li class="impressum-links"> <a href="nutzungsbedingungen.php"><img src="icons_mobil/mobile_impressum.svg"/> NUTZUNGSBEDINGUNGEN</a></li>
    </ul>

    <label for="navchanger"><img src="iconsimg/menue_desktop.svg"/></label>
</nav>

<main>
    <article class="ind">
        <h1>Nutzungsbedingungen</h1>
        <form>
            <a href="index.php" class="back">Zurück</a>
            <a href="Nutzungsbedingungen_Miabot.pdf" download>Download: AGB Stand 11/2017</a>
            <p>Die folgenden Allgemeinen Geschäftsbedingungen regeln das Vertragsverhältnis zwischen der Womik GmbH,
                Leipziger Str. 117, 01127 Dresden, Bundesrepublik Deutschland, eingetragen im Handelsregister des
                Amtsgerichts Dresden unter HRB 35556 (nachfolgend "WOMIK" genannt) und den Nutzern, die Services von
                WOMIK nutzen, und definieren die Bedingungen, unter denen die Benutzung des WOMIK-Services erfolgt. Mit
                der Anmeldung bei WOMIK z. B.: unter www.Miabot.de (nachfolgend "Website" genannt) und aller darauf
                verweisenden Domains sowie in den mobilen Apps für IOS- und Android-Geräten (nachfolgend „mobile Apps“
                genannt) erklärt sich der Nutzer mit den unten aufgeführten Geschäftsbedingungen einverstanden. Sollte
                der Nutzer mit den Geschäftsbedingungen nicht einverstanden sein, muss auf die Anmeldung verzichtet
                werden. Diese Allgemeinen Geschäftsbedingungen gelten für alle Webseiten und mobilen Apps des Betreibers
                WOMIK GmbH.</p><ol><li>Gültigkeit</li><li>Leistungsumfang</li><li>Vertragsabschluss, Vertragsbeginn</li>
                <li>Preise</li><li>Datenschutz</li><li>Beendigung der kostenlosen Mitgliedschaft</li><li>Pflichten des
                Nutzers</li><li>Verstoß gegen Nutzerpflichten</li><li>Einräumung von Rechten</li><li>Verbot gewerblicher
                oder geschäftlicher Nutzung, Verbot von Spamming</li><li>Schadensersatz</li><li>Haftung durch WOMIK</li>
                <li>Änderungen der Allgemeinen Geschäftsbedingungen</li><li>Schlussbestimmungen</li></ol>
            <h2>1. Gültigkeit</h2>
            <p>Die hier aufgeführten Geschäftsbedingungen regeln die Richtlinien, unter denen die Benutzung der Website
                WOMIK und der mobilen Apps der WOMIK GmbH erfolgt. Die Geschäftsbedingungen finden auch dann Anwendung,
                wenn der Dienst von außerhalb der Bundesrepublik Deutschland genutzt wird. Durch die Registrierung bei
                WOMIK erkennt der Nutzer an, die Allgemeinen Geschäftsbedingungen gelesen und verstanden zu haben und
                diese zu akzeptieren. Abweichende Regelungen und insbesondere Bedingungen des Nutzers, die mit den
                Geschäftsbedingungen in Widerspruch stehen, bedürfen der schriftlichen Einwilligung des Betreibers.</p>
            <h2>2. Leistungsumfang</h2>
            <p>(1) WOMIK betreibt eine Social Network Community im Internet und bietet dem Nutzer Zugriff auf eine
                Datenbank, über die sich Nutzer für den Aufbau von Freundschaften kennen lernen können. Die Datenbank
                ist unter www.Miabot.de (stellvertretend für alle darauf verweisenden Domains) sowie die mobilen Apps
                für IOS- und Android-Geräte und in Zukunft für weitere Smartphones abrufbar.<br /> Die Datenbank enthält
                Nutzer-Profile ohne Fotos und Informationen über andere Nutzer. Angemeldete Nutzer können diese Profile
                und Informationen abrufen und andere Nutzer kontaktieren.</p><p>(2) WOMIK wird ausschließlich für
                private Zwecke angeboten. Jegliche gewerbliche Nutzung ist nicht im angebotenen Leistungsumfang
                enthalten und untersagt. Gewerbliche Profile sind gekennzeichnet und tragen den Namen des jeweiligen
                Inhabers.</p><p>(3) WOMIK ist berechtigt, dritte Dienstleister und Erfüllungsgehilfen mit der Erbringung
                von Teilen oder des ganzen Leistungsspektrums zu beauftragen.</p><p>(4) Der Nutzer stimmt ausdrücklich
                zu, dass WOMIK ihm Newsletter in unregelmäßigen Abständen zusenden darf. Der Newsletter-Versand kann
                durch den Nutzer via Abmelde-Link jederzeit gestoppt werden.</p>
             <h2>3. Vertragsabschluss, Vertragsbeginn</h2>
            <p>(1) Der Vertrag zwischen WOMIK und dem Nutzer kommt bei der kostenlosen Registrierung durch den Nutzer
                auf der Webseite oder den mobilen Apps zustande.</p><p>(2) Der Nutzer kann die kostenlose Registrierung
                durch Ausfüllen des Anmeldeformulars vornehmen. Alternativ kann er sich über ConnectFunktionen von
                Drittanbietern (z.B. Facebook-Connect) registrieren. Hierbei werden ausgewählte Daten aus den jeweiligen
                Profilen des Nutzers in die WOMIK Datenbank übernommen. Bei der Registrierung über die ConnectFunktionen
                erklärt sich der Nutzer mit den jeweiligen Bedingungen der Drittanbieter einverstanden und willigt ein,
                dass gewisse Daten in die WOMIK Datenbank übernommen werden.</p><p>(3) WOMIK weist ausdrücklich darauf
                hin, dass pro Nutzer nur ein Benutzerkonto angelegt werden darf. Bestehende Benutzerkonten müssen
                gelöscht werden, bevor ein neues Benutzerkonto registriert werden kann.</p>
            <h2>4. Preise</h2>
            <p>(1) Die Nutzung des Dienstes ist kostenlos.</p><p>(2) WOMIK behält sich das Recht vor, zusätzliche
                kostenpflichtige Dienste zu einem unbestimmten Zeitpunkt anzubieten.</p>
            <h2>5. Datenschutz</h2>
            <p>Die WOMIK Datenschutz-Richtlinien sind auf der Webseite www.Miabot.de unter dem Link Datenschutz
                abrufbar.</p>
            <h2>6. Beendigung der kostenlosen Mitgliedschaft</h2>
            <p>(1) Der Nutzer ist berechtigt die Mitgliedschaft bei WOMIK jederzeit zu kündigen. Dazu findet er in
                seinen Konto-Einstellungen auf der Webseite und in den mobilen Apps eine entsprechende
                Konto-Löschen-Funktion. Auch eine Kündigung per Fax oder E-Mail ist möglich.</p><p>(2) Die
                Mitgliedschaft ist durch WOMIK ohne Einhaltung einer Kündigungsfrist außerordentlich kündbar, wenn der
                Nutzer gegen die vorliegenden Allgemeinen Nutzungsbedingungen verstößt.</p><p>(3) Darüber hinaus behält
                sich WOMIK das Recht vor, die Registrierung von Nutzern ohne Angabe von Gründen abzulehnen.</p><p>(4)
                Nach Beendigung der Mitgliedschaft werden alle Daten des Nutzers aus der WOMIK Datenbank zeitnah
                gelöscht, soweit diese nicht zu Nachweiszwecken einer rechtswidrigen Handlung des Nutzers, über die
                Vertragsbeendigung hinausgehend, benötigt werden.</p>
            <h2>7. Pflichten des Nutzers</h2>
            <p>(1) Der Nutzer ist für den Inhalt seiner Anmeldung und somit für die Daten, die er über sich macht,
                alleine verantwortlich. Insbesondere verpflichtet er sich keine rassistischen, beleidigenden,
                diskriminierenden, belästigenden, verleumderischen, sexuellen, pornografischen, gewaltverherrlichenden
                oder sonstige rechtswidrige Inhalte, Personen oder Darstellungen zu speichern, zu veröffentlichen, zu
                übermitteln und zu verbreiten. Des Weiteren wird der Nutzer ausdrücklich darauf hingewiesen, dass es
                verboten ist rechtlich geschützte Begriffe, Namen, Bilder, Videos, oder andere Materialien zu verwenden.
                </p><p>(2) Ferner ist der Nutzer verpflichtet, geeignete Maßnahmen zu treffen, um eine unautorisierte
                Nutzung seiner Daten insbesondere Kennwörter durch Dritte zu verhindern. Er verpflichtet sich, WOMIK
                unverzüglich über eine bemerkte oder vermutete unautorisierte Nutzung seines Accounts zu informieren.
                </p><p>(3) Der Nutzer versichert, dass er keine Lichtbilder, Texte oder sonstigen Werke einstellt, ohne
                nach den geltenden urheberrechtlichen Regelungen hierzu berechtigt zu sein.</p><p>(4) Darüber hinaus
                versichert der Nutzer, dass die angegebenen Informationen der Wahrheit entsprechen und seine
                Persönlichkeit beschreiben. Die Vertragspartner sind sich einig, dass es als berechtigtes Interesse für
                WOMIK gilt, die Richtigkeit der durch den Nutzer gemachten Angaben, gegebenenfalls überprüfen zu lassen.
                </p><p>(5) Der Nutzer verpflichtet sich, WOMIK schadlos von jeder Art von Klagen, Schäden, Verlusten
                oder Forderungen zu halten, die durch seine Anmeldung und / oder die Teilnahme an diesem Service
                entstehen können, insofern diese Schäden nicht auf Vorsatz oder Fahrlässigkeit WOMIK, bzw. dessen
                gesetzlicher Vertreter oder Erfüllungsgehilfen beruhen. Besonders verpflichtet sich der Nutzer, WOMIK
                von jeglicher Haftung und von allen Verpflichtungen, Aufwendungen und Ansprüchen, die sich aus Schäden
                wegen übler Nachrede, Beleidigung, Verletzung von Persönlichkeitsrechten, wegen dem Ausfall von
                Dienstleistungen für Nutzer, wegen der Verletzung von Immaterialgütern oder sonstiger Rechte ergeben,
                freizusprechen.</p><p>(6) Der Nutzer verpflichtet sich, nicht vorsätzlich die Daten dritter Personen
                (inkl. E-Mail-Adresse) als seine eigenen anzugeben. Insbesondere verpflichtet er sich dazu, nicht in
                betrügerischer Absicht die Bankverbindung oder die Kreditkartendaten Dritter anzugeben.</p><p>(7) Der
                Nutzer verpflichtet sich des Weiteren, bei Anmeldung und Nutzung des Angebots der Webseite oder der
                mobilen Apps die jeweils anwendbaren Gesetze einzuhalten.</p><p>(8) Der Nutzer verpflichtet sich auch,
                E-Mails und eventuell andere eingehende Nachrichten vertraulich zu behandeln und diese nicht, ohne die
                Zustimmung ihres Urhebers, Dritten zugänglich zu machen oder weiter zu geben. Gleiches gilt für Namen,
                Telefon- und Faxnummern, Wohn-, E-MailAdressen und / oder URLs.</p><p>(9) Ferner verpflichtet sich jeder
                einzelne Nutzer, den Service nicht missbräuchlich zu benutzen, insbesondere:<br /><ul><li>- über ihn
                    kein diffamierendes (z. B. üble Nachreden), anstößiges oder in sonstiger Weise rechtswidriges
                    Material oder solche Informationen weiterzugeben</li><li>- ihn nicht zu benutzen, um andere Personen
                    / Nutzer zu bedrohen, belästigen oder die Rechte (einschließlich Persönlichkeitsrechte) Dritter zu
                    verletzen</li><li>- keine Daten hochzuladen, die einen Virus beinhalten (infizierte Software).
                    Generell keine Software oder anderes Material hochzuladen, das urheberrechtlich geschützt ist, es
                    sei denn, der Nutzer hat die Rechte daran oder die erforderlichen Zustimmungen - hier kann durch den
                    Betreiber ein schriftlicher Nachweis gefordert werden</li><li>- ihn nicht auf eine Art und Weise zu
                    nutzen, welche die Verfügbarkeit der Leistungsangebote für andere Nutzer nachteilig beeinflusst
                    keine E-Mails abzufangen und dies auch nicht zu versuchen</li><li>- keine Werbung für andere
                    Internetportale zu betreiben - keine Kettenbriefe oder Chatnachrichten zu verfassen und an mehreren
                    Personen gleichzeitig zu versenden</li><li>- in den persönlichen und freiwilligen Angaben (Nutzer-
                    Profil) keine Namen, Adressen, Telefon- oder Faxnummern sowie E-Mail Adressen zu nennen.</li></ul>
            <h2>8. Verstoß gegen Nutzerpflichten</h2>
            <p>(1) WOMIK hat das Recht, Inhalte, Personen oder Darstellungen, die gegen die vorliegenden Allgemeinen
                Nutzungsbedingungen verstoßen oder rechtwidrig sind, z.B. gegen Gesetzes- und Rechtsvorschriften,
                insbesondere Jugendschutz, Datenschutz, Schutz des Persönlichkeitsrechts, Schutz vor Beleidigung,
                Urheberrechte, Markenrechte verstoßen selbst zu entfernen.</p><p>(2) Ein Anspruch auf Wiederherstellung
                der durch WOMIK gelöschten Informationen besteht nicht.</p><p>(3) Darüber hinaus ist WOMIK berechtigt
                den Benutzer bei Nichtbeachtung der Allgemeinen Nutzungsbedingungen zu verwarnen, temporär oder
                endgültig von den WOMIK Diensten auszuschließen. Je nach Schwere bzw. Art und Weise des Vergehens kann
                ein Verstoß auch zivil- und strafrechtliche Folgen für den Nutzer selbst mit sich bringen.</p><p>(4) Die
                Geltendmachung weiterer Ansprüche, insbesondere Schadensersatzansprüche, bleibt WOMIK ausdrücklich
                vorbehalten.</p>
            <h2>9. Einräumung von Rechten</h2>
            <p>(1) WOMIK gestattet seinen registrierten Nutzern das angebotene Produkt und Dienstleistungsportfolio unter
                Berücksichtigung der gesetzlichen Bestimmungen und der vorliegenden Allgemeinen Geschäftsbedingungen zu
                nutzen, um Inhalte hochzuladen, zu speichern, zu veröffentlichen, zu verbreiten, zu übermitteln und mit
                anderen Nutzern zu teilen.</p><p>(2) Der Nutzer willigt ein, dass WOMIK berechtigt ist auf der Basis der
                Auswertung der im Profil hinterlegten Daten sowie seines Nutzungsverhaltens auf ihn zugeschnittene
                Werbeangebote anzuzeigen.</p><p>(3) Der Nutzer räumt WOMIK an allen von ihm generierten, übermittelten,
                gespeicherten und veröffentlichten Inhalten ein unwiderrufliches, gebührenfreies, nicht exklusives,
                örtlich uneingeschränktes Nutzungsrecht ein. Demnach sind WOMIK und mit WOMIK verbundene Unternehmen zur
                uneingeschränkten Nutzung sämtlicher Inhalte, einschließlich das Bearbeiten, Kopieren, Verändern,
                Übersetzen, Erstellen und Übernahme von und in abgeleiteten Werken berechtigt. Dabei besteht keine
                Einschränkung bzgl. der Nutzungsart. WOMIK ist daher berechtigt die Inhalte für Werbezwecke oder
                sonstige Veröffentlichungen, sowohl in Teilen oder im Ganzen, in jedem Format oder Medium zu nutzen.
                Der Nutzer räumt WOMIK in diesem Zusammenhang Urheberpersönlichkeitsrechte ein, wobei WOMIK ausdrücklich
                darauf hinweist, dass WOMIK keine Inhaberschaft an der von den Nutzern bereitgestellten Inhalten erhält
                und somit keine Aufsichtsfunktion der Inhalte durch WOMIK stattfindet.</p><p>(4) WOMIK weist den Nutzer
                ausdrücklich darauf hin, dass Inhalte gespeichert und an Dritte weitergegeben werden dürfen, soweit dies
                gesetzlich vorgeschrieben oder nach pflichtgemäßem Ermessen notwendig und rechtlich zulässig ist.</p><p>
                (5) Ferner weist WOMIK ausdrücklich darauf hin, dass Nutzer-Daten zu jeder Zeit ohne Angaben von Gründen
                und ohne Benachrichtigung der betroffenen Nutzer entfernt werden können. Dies betrifft vor allem ältere
                Chatverläufe, die in unregelmäßigen Abständen aus der Datenbank entfernt werden und somit für den Nutzer
                nicht mehr abrufbar sind.</p>
            <h2>10. Verbot gewerblicher oder geschäftlicher Nutzung, Verbot von Spamming</h2>
            <p>(1) Der Nutzer versichert, dass er im Zusammenhang mit seiner Nutzung keine gewerblichen und / oder
                geschäftlichen Hintergründe verfolgt. Er verpflichtet sich, die Webseite und die mobilen Apps nicht
                gewerblich oder geschäftlich zu nutzen.</p><p>(2) Gewerbliche oder geschäftliche Benutzung, zu deren
                Unterlassung der Nutzer sich hiermit verpflichtet, sind insbesondere:</p><ul><li>- das Anbieten von
                    Waren oder Dienstleistungen gegen Geldwert, die Aufforderung ein entsprechendes Angebot abzugeben
                    oder der Verweis auf ein an anderer Stelle erreichbares entsprechendes Angebot (etwa durch Verweis
                    auf spezielle Internet-Auktionen)</li><li>- die Werbung für gewerbliche Internet-Seiten, oder mobile
                    Apps d.h. insbesondere Seiten, die:</li><ul><li>- Waren oder Dienstleistungen entgeltlich anbieten,
                        </li><li>- der Darstellung oder zur Werbung von Unternehmen dienen oder</li><li>- andere
                        gewerbliche Internet-Portale bewerben.</li></ul></ul>
            <p>Dies gilt insbesondere für Bewerbung von Unternehmen in Form von PopUps, Banner-Werbung oder durch
                besonders hervorgehobene oder auffällige Links. Eine Internet-Seite gilt insbesondere auch dann als
                gewerblich, wenn von ihr aus direkt oder indirekt auf das gewerbliche Internetportal eines anderen
                Betreibers verlinkt wird.</p><ul><li>- die Nennung bzw. Mitteilung von Mehrwertdienstleistungsnummern
                    oder Mehrwert-SMS-Nummern (Premium SMS) im Rahmen dieses Internetportals</li><li>- Kontaktaufnahme
                    zum Zweck einer anschließenden Gewinnerzielung, insbesondere durch anschließende Hinweise auf
                    0900-Nummern oder Mehrwert-SMS-Nummern.</li><li>- die Suche nach eventuellen Mitarbeitern, Modellen
                    für Agenturen oder entgeltlichen Dienstleistern</li><li>- das Sammeln der im Rahmen des
                    Internetportals zugänglichen ProfilDaten oder in Erfahrung bringen von Daten (z.B. Telefon-/
                    Handynummer) einzelner Nutzer mit dem Hintergrund der kommerziellen Benutzung, Werbung oder des
                    Weiterverkaufes an Dritte.</li></ul>
            <p>(3) Der Nutzer verpflichtet sich dazu, es zu unterlassen, anderen Nutzern des hier angebotenen Dienstes
                oder sonstigen Nutzern Werbung in jeglicher Form für kommerzielle Angebote zu machen und keine
                Nachrichten (Private Messages oder E-Mails) zu versenden, die einem gewerblichen Zweck dienen. Dies
                bezieht sich auch auf das Setzen oder Nennen von dementsprechenden Links in denen anderen Nutzern oder
                sonstigen Nutzern dieses Dienstes zugänglichen Profilen oder auf den Versand von Nachrichten mit den
                internen Nachrichtenaustausch-Systemen (z.B. Nachrichtenversand, Foren und Profildaten).</p>
            <h2>11. Schadensersatz</h2>
            <p>(1) Ein Nutzer, der durch seine Teilnahme gewerbliche oder geschäftliche Zwecke im Sinne von Punkt 10 der
                AGBs verfolgt oder nutzt, verpflichtet sich, an WOMIK einen pauschalierten Schadensersatz in Höhe von
                fünftausend Euro zu zahlen. Dem Nutzer wird in diesem Fall der Nachweis gestattet, dass ein Schaden
                überhaupt nicht entstanden oder wesentlich geringer ist als die Pauschale. WOMIK bleibt es vorbehalten
                nachzuweisen, dass ihm hierdurch ein höherer Schaden entstanden ist.</p><p>(2) Für den Fall der
                vorsätzlichen oder grob fährlässigen Verletzung der in Punkt 7 der AGBs beschriebenen Pflichten des
                Nutzers, verpflichtet sich dieser, soweit der Verstoß nicht bereits einen pauschalierten Schadensersatz
                nach Absatz 1 dieses Punktes begründet, einen Schadensersatz an den Betreiber zu zahlen. Dem Nutzer wird
                in diesem Fall der Nachweis gestattet, dass ein Schaden überhaupt nicht entstanden ist.</p>
            <h2>12. Haftung durch WOMIK</h2>
            <p>(1) Für auftretende Schäden, psychischer, physischer oder finanzieller Art, die mit dem angebotenen
                Service in Verbindung stehen, haftet WOMIK nur, insofern dieser Schaden auf einer grob fahrlässigen
                Pflichtverletzung von WOMIK, bzw. dessen gesetzlichen Vertretern oder Erfüllungsgehilfen oder auf
                Vorsatz von WOMIK, bzw. dessen gesetzlicher Vertreter oder Erfüllungsgehilfen entsteht.</p><p>(2) Für
                Schäden, die mit dem angebotenen Service in Verbindung stehen und die an anderen Rechtsgütern als Leben,
                Körper oder Gesundheit auftreten, haftet WOMIK, soweit dieser Schaden auf einer grob fahrlässigen
                Pflichtverletzung von WOMIK, bzw. dessen gesetzlicher Vertreter oder Erfüllungsgehilfen oder auf Vorsatz
                des Betreibers, bzw. dessen gesetzlicher Vertreter oder Erfüllungsgehilfen beruht.</p><p>(3) WOMIK kann
                nicht für unkorrekte Angaben in der Anmeldung des Nutzers rechtlich belangt werden, da die vom Nutzer
                gemachten Angaben aufgrund ihres Umfangreichtums nicht immer überprüft werden können und die
                Identifikation von Personen im Internet nur teilweise möglich ist. Jeder Nutzer ist selbst dafür
                verantwortlich die Angaben des Nutzers zu verifizieren, bevor er mit diesem in Form einer
                Chatunterhaltung, eines Treffens außerhalb der WOMIK Plattform oder sonstiger Interaktion mit dem Nutzer
                tritt.</p><p>(4) Ferner übernimmt WOMIK keinerlei Haftung für etwaigen Missbrauch der Informationen
                durch andere Nutzer oder Dritte, dies gilt besonders dann, wenn der Nutzer die Angaben selbst zugänglich
                macht. Außerdem haftet WOMIK nicht für unbefugte Kenntniserlangung durch Dritte von persönlichen Daten
                der Nutzer (z. B. durch unbefugten Zugriff von "Hackern" auf die Datenbank), soweit WOMIK unter
                Berücksichtigung der Einschränkung der Absätze 1 und 2 des Punkt 12 nicht eigenes Verschulden, bzw. ein
                Verschulden seiner gesetzlicher Vertreter oder Erfüllungsgehilfen trifft. WOMIK ist berechtigt, aber
                nicht verpflichtet, den Inhalt jedes Textes sowie eingesendeter / hochgeladener Fotos bzw. Grafikdateien
                auf die, diesen AGB zu Grunde liegenden Bestimmungen hin zu überprüfen und, falls erforderlich, zu
                ändern oder zu entfernen / löschen.</p><p>(5) WOMIK kann für signifikante Beeinträchtigungen des
                Dienstes nicht haftbar gemacht werden.</p><p>(6) Des Weiteren haftet WOMNIK nicht für von diesem nicht
                zu verantwortende Ausfälle des Dienstes, z. B. durch höhere Gewalt oder technischen Störungen des
                Internets.</p><p>(7) Für die Inhalte Dritter sowie externer Links kann WOMIK keine Verantwortung
                übernehmen. Bei von den Nutzern verfassten und veröffentlichten Inhalten handelt es sich um fremde
                Inhalte im Sinne des § 5 des Teledienstegesetzes (TDG) in der Fassung vom 22.7.1997, weshalb für diese
                von WOMIK keinerlei Verantwortung übernommen wird.</p>
            <h2>13. Änderungen der Allgemeinen Geschäftsbedingungen</h2>
            <p>(1) WOMIK behält sich das Recht vor, die allgemeinen Geschäftsbedingungen zu ändern.</p><p>(2) Der Nutzer
                erklärt, mit der Anwendung der geänderten Allgemeinen Geschäftsbedingungen (AGB) auf bereits vor der
                Änderung geschlossene Verträge einverstanden zu sein, wenn WOMIK den Nutzer darauf hinweist, dass eine
                Änderung der AGB stattgefunden hat und der Nutzer nicht innerhalb einer Frist von zwei Wochen, beginnend
                mit dem Tag, der auf die Änderungsmitteilung folgt, der Abänderung widerspricht.</p><p>(3) Die
                Mitteilung der Änderung muss noch einmal den Hinweis auf die Möglichkeit und Frist des Widerspruchs,
                sowie die Bedeutung, bzw. Folgen des Unterlassens eines Widerspruches enthalten. Sie kann per E-Mail an
                die vom Nutzer angegebene E-Mail-Adresse erfolgen.</p>
            <h2>14. Streitbeilegung</h2>
            <p>(1) Die EU-Kommission stellt eine Plattform zur Online-Streitbeilegung (OSPlattform) bereit. Diese ist
                hier zu finden: www.ec.europa.eu/consumers/odr. WOMIK ist zur Teilnahme an einem
                Streitbeilegungsverfahren vor einer Verbraucherschlichtungsstelle weder verpflichtet noch bereit.</p>
            <h2>15. Schlussbestimmungen</h2>
            <p>(1) Vertragsabänderungen, Ergänzungen und Nebenabreden bedürfen, sofern in diesen Allgemeinen
                Geschäftsbedingungen nichts anderes bestimmt ist, zu ihrer Wirksamkeit der Schriftform beider Seiten.
                Das Schriftformerfordernis gilt auch für den Verzicht auf dieses Schriftformerfordernis.</p><p>(2) Die
                vertraglichen Vereinbarungen der Vertragspartner unterliegen dem Recht der Bundesrepublik Deutschland
                unter Ausschluss des UN-Kaufrechts (CISG) und des Kollisionsrechts.</p><p>(3) Sollten einzelne
                Bestimmungen dieses Vertrages unwirksam oder undurchführbar sein oder nach Vertragsschluss unwirksam
                oder undurchführbar werden, bleibt davon die Wirksamkeit des Vertrages im Übrigen unberührt. An die
                Stelle der unwirksamen oder undurchführbaren Bestimmung soll diejenige wirksame und durchführbare
                Regelung treten, deren Wirkungen der wirtschaftlichen Zielsetzung am nächsten kommen, die die
                Vertragsparteien mit der unwirksamen bzw. undurchführbaren Bestimmung verfolgt haben. Die vorstehenden
                Bestimmungen gelten entsprechend für den Fall, dass sich der Vertrag als lückenhaft erweist.</p>
            <p>AGB Stand 11/2017</p>
        </form>
    </article>
</main>

<footer>
    <div id="imp">
        <a class="one" href="impressum.php">Impressum</a>
        <a class="one" href="nutzungsbedingungen.php">Nutzungsbedingungen</a>
        <a class="two" href="datenschutz.php">Datenschutz</a>
    </div>
    <div class="made">© 2017 MIABOT - Made in Dresden</div>
</footer>
</body>
</html>