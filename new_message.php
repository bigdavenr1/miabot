<?php
error_reporting(E_ALL ^ E_NOTICE);
session_start();

if(empty($_SESSION['user']->id)):
    header('LOCATION:index.php');
    exit;
endif;

//Load the database configuration file
include 'dbConfig.php';
//if(!empty ($_POST['ins_meer_werfen'])):
if(isset($_POST['ins_meer_werfen']) && !empty($_POST['flaschenpost'])):
    $_POST['status'] = "my status";
    $query = "INSERT INTO flaschenpost (user, location, content, status) VALUES (".$_SESSION['user']->id.", '".$_POST['location']."', '".$_POST['flaschenpost']."', '".$_POST['status']."')";
    if ($db->query($query) === TRUE) {
        echo "Flaschenpost erfolgreich hinzugefügt!";
        header('LOCATION:my_messages.php');
    } else {
        echo "Error: " . $query . "<br>" . $db->error;
    }

    $db->close();
endif;
if(!empty ($_POST['flaschenpost'])):
    $query = "INSERT INTO flaschenpost SET location = '".$_POST['location']."', content = '".$_POST['flaschenpost']."'";
endif;
?>

<!DOCTYPE html>
<html lang="de">
<head>
    <title>Miabot.de</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

</head>

<body onload="findMe()">
<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '125564491479635',
            xfbml      : true,
            version    : 'v2.12'
        });

        FB.AppEvents.logPageView();
    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<!-- Facebook Pixel Code -->
<script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window,document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '170965336587525');
    fbq('track', 'PageView');
    fbq('track', 'ViewContent');
</script>
<noscript>
    <img height="1" width="1" src="https://www.facebook.com/tr?id=170965336587525&ev=PageView&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDwQtj0GJYgS9Fafxkwn0iyWt5oRKfoEh0">
</script>
<script>
    "use strict";
    function findMe() {
        if(navigator.geolocation){
            navigator.geolocation.getCurrentPosition(geocodeLatLng, errorCallback);
        } else {
            let span = document.querySelector('span.alert');
            span.innerHTML = '<strong>Geolocation nicht verfügbar!</strong><br />' +
                'Du verwendest keinen aktuellen Browser! Das Geolocation-Feature ist jedoch dringen erforderlich um den ort zu ermitteln<br />' +
                'an dem deine Flaschenpost ins Meer geworfen wurde.<br />Ohne diese Funktion kann die Flaschenpost nicht abgesendet werden!<br/><strong>Es ist kein Rückschluss auf die genaue Position möglich!</strong>';
            span.style.display = "block";
        }
    }

    function errorCallback(error) {
        if (error.code == error.PERMISSION_DENIED) {

            let span = document.querySelector('span.alert');
            span.innerHTML = '<strong>Geolocation bitte aktivieren!</strong><br />' +
                'Diese Funktion wird benötigt um den Ort zu bestimmen<br />' +
                'an dem deine Flaschenpost ins Meer geworfen wurde.<br />Ohne diese Funktion kann die Flaschenpost nicht abgesendet werden!<br/><strong>Es ist kein Rückschluss auf die genaue Position möglich!</strong>';
            span.style.display = "block";
        }
    }


    function geocodeLatLng(position) {

        var geocoder = new google.maps.Geocoder;

        var latlng = {lat: position.coords.latitude, lng: +position.coords.longitude};
        geocoder.geocode({'location': latlng}, function(results, status) {
            if (status === 'OK') {
                console.info(results);
                if (results[3]) {
                    document.querySelector('#geo').innerText = results[3].formatted_address;
                    document.getElementById('geo_input').value = results[3].formatted_address;
                } else {
                    let span = document.querySelector('span.alert');
                    span.innerHTML = '<strong>Geolocation Fehler!</strong><br />' +
                        'Es ist ein Fehler bei der Standortbestimmung aufgetreten.<br />' +
                        'Bitte lade diese Seite neu! Ohne Standortbestiummung kannst du die Flaschenpost nicht absenden!<br/><strong>Es ist kein Rückschluss auf die genaue Position möglich!</strong>';
                    span.style.display = "block";
                }
            } else {
                let span = document.querySelector('span.alert');
                span.innerHTML = '<strong>Geolocation Fehler!</strong><br />' +
                    'Es ist ein Fehler bei der Standortbestimmung aufgetreten.<br />' +
                    'Bitte lade diese Seite neu! Ohne Standortbestiummung kannst du die Flaschenpost nicht absenden!<br/><strong>Es ist kein Rückschluss auf die genaue Position möglich!</strong>';
                span.style.display = "block";
            }
        });
    }
    // Wenn die Seite fertig geladen ist
    document.addEventListener('DOMContentLoaded', function(){
        // erlaubte Anzahl setzen
        let erlaubteAnzahl = 240,
            // input und Zählerelement wählen
            input = document.querySelector('textarea[name="flaschenpost"]'),
            zaehler = document.querySelector('#character');
        // Eventlistzener wenn Taste gedrückt ist
        input.addEventListener('keydown', function(e){

            // prüfen ob STRG gedrückt wurde und ob nur ein Zeichen oder evtl. Enter gedrückt wurde
            if( e.ctrlKey === false && ( e.key.length === 1 || e.key === "Enter" ) ) {
                // Standardaktion verhindern
                if(this.value.length < erlaubteAnzahl) {
                    // Wenn Enter gedrückt wurde
                    if (e.key === "Enter") {
                        // neue Zeile einfügen. Aber maximal 240 Zeichen
                        this.value = (this.value + "\n").substr(0, erlaubteAnzahl - 1);
                        // Und nach ganz unten scrollen (geht sonst nicht automatisch)
                        this.scrollTop = this.scrollHeight;
                        //Wenn andere gültige Taste gedrückt wurde
                    } else {
                        // Zecihen einfügen. Aber maximal 240 Zeichen
                        this.value = (this.value).substr(0, erlaubteAnzahl - 1);
                    }
                }else {e.preventDefault();}

            }

            zaehler.innerText = this.value.length;
        });
        // Event listener wenn Taste losgelassen wird
        input.addEventListener( 'keyup' , function(){
            // Text des zähler anpassen
            zaehler.innerText = this.value.length;
        });
        //Eventlistener STRG + V
        input.addEventListener( 'paste' , function( e ){
            // Daten aus dem Clipboard lesen (Sonderweg für IE)
            let data = e.clipboardData === undefined ? window.clipboardData.getData('Text'):e.clipboardData.getData('Text');
            // Standardaktiuon verhindern
            e.preventDefault();
            // Text einfügen (aber maximal 240 Zeichen)
            this.value = ( this.value + data ).substr( 0 , erlaubteAnzahl );
            zaehler.innerText = this.value.length;
        });
    });
</script>

<header>
    <a href="start_page.php">
        <img src="icons_mobil/miabot_ueberschrift_mobile.svg"/>
        <img src="miabot_ueberschrift+logo_desktop.svg"/>
    </a>
</header>

<input type="checkbox" id="navchanger">
<nav>
    <ul id="menu_top">
        <li> <a href="new_message.php"><img src="iconsimg/neue_fp.svg"/> NEUE FLASCHENPOST</a></li>
        <li> <a href="my_messages.php"><img src="iconsimg/pfeil_eigene_fp.svg"/> EIGENE FLASCHENPOST</a></li>
        <li> <a href="received_message.php"><img src="iconsimg/pfeil_erhaltene_fp.svg"/> GEFUNDENE FLASCHENPOST</a></li>
    </ul>

    <ul id="menu_bottom">
        <li class="fb-logout <?php echo !empty($_SESSION['user']) ? 'eingeloggt' : 'ausgeloggt';?>"> <a href="index.php?action=logout">
                <img src="iconsimg/fb-art_sml.png"/> LOGOUT
            </a></li>
        <li class="impressum-links"> <a href="impressum.php"><img src="icons_mobil/mobile_impressum.svg"/> IMPRESSUM</a></li>
        <li class="impressum-links"> <a href="datenschutz.php"><img src="icons_mobil/mobile_impressum.svg"/> DATENSCHUTZ</a></li>
        <li class="impressum-links"> <a href="nutzungsbedingungen.php"><img src="icons_mobil/mobile_impressum.svg"/> NUTZUNGSBEDINGUNGEN</a></li>
    </ul>

    <label for="navchanger"><img src="iconsimg/menue_desktop.svg"/></label>
</nav>


<main>
    <article id="newmessage">
        <h1>Flaschenpost verfassen</h1>
        <form method="post" action="new_message.php">
            <div class="textarea">
                <textarea placeholder="Hier Text eingeben..." name="flaschenpost"></textarea>
                <span class="count"><span id="character">0</span><span> / 240 Zeichen</span></span>
            </div><br/>
            <br/>

            <span>Deine Nachricht wird versendet aus:</span><br/><span id="geo"></span>
            <span class="alert"></span>
            <input name="location" id="geo_input" type="hidden" value="" />

            <input type="submit" name="ins_meer_werfen" value="INS MEER WERFEN"/>
        </form>
    </article>
</main>

<footer>
    <div id="imp">
        <a class="one" href="impressum.php">Impressum</a>
        <a class="one" href="nutzungsbedingungen.php">Nutzungsbedingungen</a>
        <a class="two" href="datenschutz.php">Datenschutz</a>
    </div>
    <div class="made">© 2017 MIABOT - Made in Dresden</div>
</footer>

<?php
if(isset($_GET['new'])){ ?>
    <main class="hint">
        <article>
            <h1>Das Miabot Prinzip</h1>
            <form>
                <div>

                    Nur wer eine Flaschenpost schreibt, kann auch eine bekommen.<br><br>

                    Du kannst nicht beeinflussen, wer deine Flaschenpost findet. Es ist alles Zufall - wie bei einer echten Flaschenpost.<br><br>

                    Deine Flaschenpost wird völlig anonym versendet. Es wird nur dein Ort angezeigt.<br><br>

                    Und denke immer dran - eine ordentliche Kommunikation ist der Schlüssel zu jeder guten Beziehung!<br><br>

                </div>
                <a href="new_message.php">Jetzt Flaschenpost schreiben</a>
            </form>
        </article>
    </main>
<?php }
?>



</body>
</html>