<?php
error_reporting(E_ALL ^ E_NOTICE);
session_start();

if(empty($_SESSION['user']->id)):
    header('LOCATION:index.php');
    exit;
endif;

//Load the database configuration file
include 'dbConfig.php';

// Texr eingeben Event
if(isset($_POST['abschicken']) && !empty($_POST['content'])):
    $query = "INSERT INTO antwort (user, flaschenpost, content, location, op) VALUES (".$_SESSION['user']->id.", ".$_GET['fid'].", '".$_POST['content']."', '".$_POST['location']."', 1)";

    if ($db->query($query) !== TRUE) {
        echo "Error: " . $query . "<br>" . $db->error;
    }
    //$db->close();
endif;

// Chatverlauf laden
$query = "SELECT * FROM antwort WHERE flaschenpost = ".$_GET['fid']."  order by date";
$objslist = $db->query($query);

$query = "SELECT * FROM flaschenpost WHERE id = ".$_GET['fid'];
$flaschenpostobj = $db->query($query);
$flaschenpost = $flaschenpostobj->fetch_object();
?>

<!DOCTYPE html>
<html lang="de">
<head>
    <title>Miabot.de</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

</head>

<body>
<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '125564491479635',
            xfbml      : true,
            version    : 'v2.12'
        });

        FB.AppEvents.logPageView();
    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<!-- Facebook Pixel Code -->
<script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window,document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '170965336587525');
    fbq('track', 'PageView');
    fbq('track', 'ViewContent');
</script>
<noscript>
    <img height="1" width="1" src="https://www.facebook.com/tr?id=170965336587525&ev=PageView&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->
<header>
    <a href="start_page.php">
        <img src="icons_mobil/miabot_ueberschrift_mobile.svg"/>
        <img src="miabot_ueberschrift+logo_desktop.svg"/>
    </a>
</header>

<input type="checkbox" id="navchanger">
<nav>
    <ul id="menu_top">
        <li> <a href="new_message.php"><img src="iconsimg/neue_fp.svg"/> NEUE FLASCHENPOST</a></li>
        <li> <a href="my_messages.php"><img src="iconsimg/pfeil_eigene_fp.svg"/> EIGENE FLASCHENPOST</a></li>
        <li> <a href="received_message.php"><img src="iconsimg/pfeil_erhaltene_fp.svg"/> GEFUNDENE FLASCHENPOST</a></li>
    </ul>

    <ul id="menu_bottom">
        <li> <a href="settings.php"><img src="iconsimg/einstellungen.svg"/> EINSTELLUNGEN</a></li>
        <li> <a href="index.php?action=logout"><img src="iconsimg/fb-art_sml.png"/> LOGOUT</a></li>
    </ul>

    <label for="navchanger"><img src="iconsimg/menue_desktop.svg"/></label>
</nav>

<main>
    <article id="chat">
        <h1><a href="received_message.php"><img src="iconsimg/zurueck_pfeil.svg"/></a>Flaschenpost aus Hannover vom 29.12.2017 - 16 Uhr</h1>
        <form>
            <input type="checkbox" id="originalchanger">
            <div class="original_fp">
                <p>Hier würde jetzt meine erste Flaschenpost stehen, oder zumindest ihr Blindtext.
                    Diese Zeile wird man nicht richtig lesen können…
                    Um 240 Zeichen voll zu bekommen, schreibe ich jetzt weiter. Mehr Platz muss für die original FP nicht da sein.</p>
                <ul>
                    <li> <a href="">ZURÜCK INS MEER<img src="iconsimg/zurueck_ins_meer.svg"/></a> </li>
                    <li> <a href="">MELDEN<img src="iconsimg/melden.svg"/></a> </li>
                </ul>
                <div class="linear_gradient"></div>

                <label for="originalchanger"></label>
            </div>

            <div class="scroll-view">
                <div class="chatarea">
                    <div class="day">29. Dezember 2017</div>

                    <div class="other">
                        <p>SOS!!!</p>
                        <p>Ich brauch bei ... unbedingt Hilfe von irgendwem!</p>
                        <p>Bitte schreibt schnell zurück! Würde mich unglaublich freuen</p>
                        <time><img src="iconsimg/uhr_chat.svg"/>16:00</time>
                    </div>

                    <div class="day">Gestern</div>

                    <div class="self">
                        <p>Hey!</p>
                        <p>Wie kann ich dir helfen?</p>
                        <time><img src="iconsimg/uhr_chat.svg"/>23:49</time>
                    </div>

                    <div class="other">
                        <p>Oh Gott, endlich antwortet jemand!!</p>
                        <p>Also hör zu, es geht um folgendes...</p>
                        <time><img src="iconsimg/uhr_chat.svg"/>23:52</time>
                    </div>

                    <div class="day">Heute</div>

                    <div class="other">
                        <p>Bitte antworte so schnell es geht! :(</p>
                        <time><img src="iconsimg/uhr_chat.svg"/>07:03</time>
                    </div>
                </div>
            </div>

            <div class="textarea">
                <textarea placeholder="Hier Text eingeben..." maxlength="240" spellcheck="true"></textarea>
                <input type="submit" value="" />
            </div>
        </form>
    </article>
</main>

<footer>
    <div id="imp">
        <a class="one" href="impressum.php">Impressum</a>
        <a class="one" href="nutzungsbedingungen.php">Nutzungsbedingungen</a>
        <a class="two" href="datenschutz.php">Datenschutz</a>
    </div>
    <div class="made">© 2017 MIABOT - Made in Dresden</div>
</footer>

</body>
</html>