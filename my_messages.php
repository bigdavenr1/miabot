<?php
error_reporting(E_ALL ^ E_NOTICE);
session_start();

if(empty($_SESSION['user']->id)):
    header('LOCATION:index.php');
    exit;
endif;

//Load the database configuration file
include 'dbConfig.php';

// eingene Flaschenpost mit deren antworten
$query = "SELECT * FROM flaschenpost f WHERE f.user = ".$_SESSION['user']->id." order by date desc";
$objslist = $db->query($query);

$query = "SELECT COUNT(user) anzahl FROM flaschenpost WHERE user = ".$_SESSION['user']->id;
$anzahlobj = $db->query($query);
$anzahl = $anzahlobj->fetch_object();
if( empty($anzahl->anzahl)) :
    header('LOCATION:new_message.php?new');
    exit;
endif;
?>

<!DOCTYPE html>
<html lang="de">
<head>
    <title>Miabot.de</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

</head>

<body>
<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '125564491479635',
            xfbml      : true,
            version    : 'v2.12'
        });

        FB.AppEvents.logPageView();
    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<!-- Facebook Pixel Code -->
<script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window,document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '170965336587525');
    fbq('track', 'PageView');
    fbq('track', 'ViewContent');
</script>
<noscript>
    <img height="1" width="1" src="https://www.facebook.com/tr?id=170965336587525&ev=PageView&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->
<header>
    <a href="start_page.php">
        <img src="icons_mobil/miabot_ueberschrift_mobile.svg"/>
        <img src="miabot_ueberschrift+logo_desktop.svg"/>
    </a>
</header>

<input type="checkbox" id="navchanger">
<nav>
    <ul id="menu_top">
        <li> <a href="new_message.php"><img src="iconsimg/neue_fp.svg"/> NEUE FLASCHENPOST</a></li>
        <li> <a href="my_messages.php"><img src="iconsimg/pfeil_eigene_fp.svg"/> EIGENE FLASCHENPOST</a></li>
        <li> <a href="received_message.php"><img src="iconsimg/pfeil_erhaltene_fp.svg"/> GEFUNDENE FLASCHENPOST</a></li>
    </ul>

    <ul id="menu_bottom">
        <li class="fb-logout <?php echo !empty($_SESSION['user']) ? 'eingeloggt' : 'ausgeloggt';?>"> <a href="index.php?action=logout">
                <img src="iconsimg/fb-art_sml.png"/> LOGOUT
            </a></li>
        <li class="impressum-links"> <a href="impressum.php"><img src="icons_mobil/mobile_impressum.svg"/> IMPRESSUM</a></li>
        <li class="impressum-links"> <a href="datenschutz.php"><img src="icons_mobil/mobile_impressum.svg"/> DATENSCHUTZ</a></li>
        <li class="impressum-links"> <a href="nutzungsbedingungen.php"><img src="icons_mobil/mobile_impressum.svg"/> NUTZUNGSBEDINGUNGEN</a></li>
    </ul>

    <label for="navchanger"><img src="iconsimg/menue_desktop.svg"/></label>
</nav>

<main>
    <article id="mymessages">
        <h1>Eigene Flaschenpost</h1>

        <?php
        if ($objslist->num_rows > 0):
            while($row = $objslist->fetch_object()):?>
                <form>
                    <div class="fp">
                        <?php

                        $query = "SELECT COUNT(id) anzahl FROM antwort WHERE flaschenpost = ".$row->id;
                        $anzahlanworten = $db->query($query);
                        $anzahl = $anzahlanworten->fetch_object();
                        ?>
                        <h2 class="<?php echo ($anzahl->anzahl > 0) ? "hasMessage" : "hasNoMessage"; ?>">
                            <span>DEINE FLASCHENPOST VOM <?php $date = date_create($row->date); echo $date->format('d.m.Y') ?></span>
                            <span><?php echo $anzahl->anzahl ?></span>
                        </h2>
                        <p><?php echo nl2br($row->content) ?></p>
                    </div><div class="new">
                        <h2 class="<?php echo ($anzahl->anzahl > 0) ? "hasMessage" : "hasNoMessage"; ?>"><?php echo $anzahl->anzahl ?>
                            <span>NACHRICHTEN</span>
                        </h2>
                    </div>

                    <input type="checkbox" id="antwortenchanger<?php echo $row->id?>" class="antwortenchanger">
                    <ul class="antworten">
                        <?php
                        $query = "SELECT * FROM antwort WHERE flaschenpost = ".$row->id;
                        $anworten = $db->query($query);
                        $index = 1;
                        while($antwortrow = $anworten->fetch_object()): ?>

                            <li class="<?php echo (($index % 2) == 1) ? "alternate" : ""; ?> <?php echo ($antwortrow->read == 1) ? "read" : ""; ?>">
                                <a href="chatverlauf_eigene.php?fid=<?php echo $row->id ?>">
                                    ANTWORT AUS <?php echo $antwortrow->location ?> VOM <?php echo $antwortrow->date ?>
                                </a>
                            </li>

                        <?php $index++;
                        endwhile; ?>
                    </ul>
                    <label for="antwortenchanger<?php echo $row->id?>"></label>
                </form>
                <?php
            endwhile;
        endif;
        ?>
    </article>
</main>

<footer>
    <div id="imp">
        <a class="one" href="impressum.php">Impressum</a>
        <a class="one" href="nutzungsbedingungen.php">Nutzungsbedingungen</a>
        <a class="two" href="datenschutz.php">Datenschutz</a>
    </div>
    <div class="made">© 2017 MIABOT - Made in Dresden</div>
</footer>

<?php
if(isset($_GET['new'])){ ?>
    <main class="hint">
        <article>
            <h1>Das Miabot Prinzip</h1>
            <form>
                <div>

                    Nur wer eine Flaschenpost schreibt, kann auch eine bekommen.<br><br>

                    Du kannst nicht beeinflussen, wer deine Flaschenpost findet. Es ist alles Zufall - wie bei einer echten Flaschenpost.<br><br>

                    Deine Flaschenpost wird völlig anonym versendet. Es wird nur dein Ort angezeigt.<br><br>

                    Und denke immer dran - eine ordentliche Kommunikation ist der Schlüssel zu jeder guten Beziehung!<br><br>

                </div>
                <a href="new_message.php">Jetzt Flaschenpost schreiben</a>
            </form>
        </article>
    </main>
<?php }
?>

</body>
</html>