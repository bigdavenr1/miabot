<?php
error_reporting(E_ALL ^ E_NOTICE);
session_start();
?>
<!DOCTYPE html>
<html lang="de">
<head>
    <title>Miabot.de</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

</head>

<body>
<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '125564491479635',
            xfbml      : true,
            version    : 'v2.12'
        });

        FB.AppEvents.logPageView();
    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<!-- Facebook Pixel Code -->
<script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window,document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '170965336587525');
    fbq('track', 'PageView');
    fbq('track', 'ViewContent');
</script>
<noscript>
    <img height="1" width="1" src="https://www.facebook.com/tr?id=170965336587525&ev=PageView&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->
<header>
    <a href="start_page.php">
        <img src="icons_mobil/miabot_ueberschrift_mobile.svg"/>
        <img src="miabot_ueberschrift+logo_desktop.svg"/>
    </a>
</header>

<input type="checkbox" id="navchanger">
<nav class="<?php echo !empty($_SESSION['user']) ? 'eingeloggt' : 'ausgeloggt';?>">
    <ul id="menu_top">
        <li class="<?php echo !empty($_SESSION['user']) ? 'eingeloggt' : 'ausgeloggt';?>"> <a href="new_message.php">
                <img src="iconsimg/neue_fp.svg"/> NEUE FLASCHENPOST
            </a></li>
        <li class="<?php echo !empty($_SESSION['user']) ? 'eingeloggt' : 'ausgeloggt';?>"> <a href="my_messages.php">
                <img src="iconsimg/pfeil_eigene_fp.svg"/> EIGENE FLASCHENPOST
            </a></li>
        <li class="<?php echo !empty($_SESSION['user']) ? 'eingeloggt' : 'ausgeloggt';?>"> <a href="received_message.php">
                <img src="iconsimg/pfeil_erhaltene_fp.svg"/> GEFUNDENE FLASCHENPOST
            </a></li>
    </ul>

    <ul id="menu_bottom">
        <li class="fb-logout <?php echo !empty($_SESSION['user']) ? 'eingeloggt' : 'ausgeloggt';?>"> <a href="index.php?action=logout">
                <img src="iconsimg/fb-art_sml.png"/> LOGOUT
            </a></li>
        <li class="impressum-links"> <a href="impressum.php"><img src="icons_mobil/mobile_impressum.svg"/> IMPRESSUM</a></li>
        <li class="impressum-links"> <a href="datenschutz.php"><img src="icons_mobil/mobile_impressum.svg"/> DATENSCHUTZ</a></li>
        <li class="impressum-links"> <a href="nutzungsbedingungen.php"><img src="icons_mobil/mobile_impressum.svg"/> NUTZUNGSBEDINGUNGEN</a></li>
    </ul>

    <label for="navchanger"><img src="iconsimg/menue_desktop.svg"/></label>
</nav>

<main>
    <article class="ind">
        <h1>Impressum</h1>
        <form>
            <a href="index.php" class="back">Zurück</a>
            <h2>Angaben gem&auml;&szlig; &sect; 5 TMG:</h2> <p>Womik GmbH<br />
                Leipziger Str. 117<br /> 01127 Dresden</p> <h2>Vertreten durch:</h2> <p>Gesch&auml;ftsf&uuml;hrer
                Heiko Roch</p> <h2>Kontakt:</h2> <p>Telefon: +49 (0) 351 42640 505<br /> Telefax: +49 (0) 351 42640
                506<br /> E-Mail: aloho@aloho.de</p> <h2>Registereintrag:</h2> <p>Eintragung im Handelsregister. <br
                /> Registergericht:Amtsgericht Dresden <br /> Registernummer: HRB 35556 </p> <h2>
                Umsatzsteuer:</h2> <p>Umsatzsteuer-Identifikationsnummer gem&auml;&szlig; &sect;27 a
                Umsatzsteuergesetz:<br /> DE307599077</p> <h2>Verantwortlich f&uuml;r den Inhalt nach &sect; 55
                Abs. 2 RStV:</h2> <p>Heiko Roch<br /> Leipziger Str. 117<br /> 01127 Dresden</p> <h2>

                Streitschlichtung</h2> <p>Die Europ&auml;ische Kommission stellt eine Plattform zur Online-
                Streitbeilegung (OS) bereit: <a href="http://ec.europa.eu/consumers/odr">

                    http://ec.europa.eu/consumers/odr</a><br /> Unsere E-Mail-Adresse finden Sie im Impressum.</p>
            <p>Wir sind nicht bereit oder verpflichtet, an Streitbeilegungsverfahren vor einer
                Verbraucherschlichtungsstelle teilzunehmen.</p> <h2>Haftung f&uuml;r Inhalte</h2> <p>Als
                Diensteanbieter sind wir gem&auml;&szlig; &sect; 7 Abs.1 TMG f&uuml;r eigene Inhalte auf diesen Seiten
                nach den allgemeinen Gesetzen verantwortlich. Nach &sect;&sect; 8 bis 10 TMG sind wir als
                Diensteanbieter jedoch nicht verpflichtet, &uuml;bermittelte oder gespeicherte fremde Informationen zu
                &uuml;berwachen oder nach Umst&auml;nden zu forschen, die auf eine rechtswidrige T&auml;tigkeit
                hinweisen.</p> <p>Verpflichtungen zur Entfernung oder Sperrung der Nutzung von Informationen nach
                den allgemeinen Gesetzen bleiben hiervon unber&uuml;hrt. Eine diesbez&uuml;gliche Haftung ist jedoch
                erst ab dem Zeitpunkt der Kenntnis einer konkreten Rechtsverletzung m&ouml;glich. Bei Bekanntwerden
                von entsprechenden Rechtsverletzungen werden wir diese Inhalte umgehend entfernen.</p> <h2>
                Haftung f&uuml;r Links</h2> <p>Unser Angebot enth&auml;lt Links zu externen Webseiten Dritter, auf
                deren Inhalte wir keinen Einfluss haben. Deshalb k&ouml;nnen wir f&uuml;r diese fremden Inhalte auch
                keine Gew&auml;hr &uuml;bernehmen. F&uuml;r die Inhalte der verlinkten Seiten ist stets der jeweilige
                Anbieter oder Betreiber der Seiten verantwortlich. Die verlinkten Seiten wurden zum Zeitpunkt der
                Verlinkung auf m&ouml;gliche Rechtsverst&ouml;&szlig;e &uuml;berpr&uuml;ft. Rechtswidrige Inhalte
                waren zum Zeitpunkt der Verlinkung nicht erkennbar.</p> <p>Eine permanente inhaltliche Kontrolle der
                verlinkten Seiten ist jedoch ohne konkrete Anhaltspunkte einer Rechtsverletzung nicht zumutbar. Bei
                Bekanntwerden von Rechtsverletzungen werden wir derartige Links umgehend entfernen.</p> <h2>
                Urheberrecht</h2> <p>Die durch die Seitenbetreiber erstellten Inhalte und Werke auf diesen Seiten
                unterliegen dem deutschen Urheberrecht. Die Vervielf&auml;ltigung, Bearbeitung, Verbreitung und jede
                Art der Verwertung au&szlig;erhalb der Grenzen des Urheberrechtes bed&uuml;rfen der schriftlichen
                Zustimmung des jeweiligen Autors bzw. Erstellers. Downloads und Kopien dieser Seite sind nur f&uuml;r
                den privaten, nicht kommerziellen Gebrauch gestattet.</p> <p>Soweit die Inhalte auf dieser Seite nicht
                vom Betreiber erstellt wurden, werden die Urheberrechte Dritter beachtet. Insbesondere werden Inhalte
                Dritter als solche gekennzeichnet. Sollten Sie trotzdem auf eine Urheberrechtsverletzung aufmerksam
                werden, bitten wir um einen entsprechenden Hinweis. Bei Bekanntwerden von Rechtsverletzungen
                werden wir derartige Inhalte umgehend entfernen.</p>

        </form>
    </article>
</main>

<footer>
    <div id="imp">
        <a class="one" href="impressum.php">Impressum</a>
        <a class="one" href="nutzungsbedingungen.php">Nutzungsbedingungen</a>
        <a class="two" href="datenschutz.php">Datenschutz</a>
    </div>
    <div class="made">© 2017 MIABOT - Made in Dresden</div>
</footer>
</body>
</html>