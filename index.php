<?php
require_once "config.php";

$redirectURL = "https://miabot.de/fb-callback.php";
$permissions = ['email'];
$loginURL = $helper->getLoginUrl($redirectURL, $permissions);

//session_destroy();
?>

<?php
//error_reporting(E_ALL ^ E_NOTICE);
//session_start();

//Load the database configuration file
include 'dbConfig.php';

if($_GET['action']=== 'logout') :
    unset($_SESSION['user']);
endif;
//var_dump($_SESSION);
if(!empty($_SESSION['Fingerprint'])):
    unset($_SESSION['Fingerprint']);    ?>
    <script src="fingerprint2.js"></script>
    <script>
        new Fingerprint2().get(function(result, components){
            $.post('index.php', {oauth_provider:'facebook',userData: JSON.stringify(userData),fingerprint:result, pcData: JSON.stringify(components)}, function(data){ return true; })
                .done(function (data) {
                    console.info(data);
                    $('body').html(data);
                });
        });
    </script>
<?php endif;

if(!empty($_SESSION['user']->id)):

    if($_SESSION['loggedin']==="yes"):
        $query = "INSERT INTO user_login SET domain='".$_SERVER['HTTP_HOST']."', id = ".$_SESSION['user']->id.", fingerprint= '".$_POST['fingerprint']."' , pcdata='".$_POST['pcData']."'";
        $insert = $db->query($query);
        unset($_SESSION['loggedin']);
    endif;

    $query = "SELECT COUNT(user) anzahl FROM flaschenpost WHERE user = ".$_SESSION['user']->id;
    $anzahlobj = $db->query($query);
    $anzahl = $anzahlobj->fetch_object();
    if($anzahl->anzahl > 0) :
        header('LOCATION:start_page.php');
        exit;
    else :
        header('LOCATION:new_message.php?new');
        exit;
    endif;

endif;

?>

<!DOCTYPE html>
<html>
<head>
    <title>Miabot.de</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

</head>

<body>
<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '125564491479635',
            xfbml      : true,
            version    : 'v2.12'
        });

        FB.AppEvents.logPageView();
    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<!-- Facebook Pixel Code -->
<script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window,document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '170965336587525');
    fbq('track', 'PageView');
    fbq('track', 'ViewContent');
</script>
<noscript>
    <img height="1" width="1" src="https://www.facebook.com/tr?id=170965336587525&ev=PageView&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="fingerprint2.js"></script>

<div id="fb-root"></div>
<script>
    /*(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/de_DE/sdk.js#xfbml=1&version=v2.11&appId=125564491479635&auth_type=rerequest';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    // Facebook login with JavaScript SDK
    function fbLogin() {
        var popUp = window.open('', 'test', 'width=1, height=1, left=-24, top=-24, scrollbars=no, resizable=no');
        if (popUp == null || typeof(popUp)=='undefined') {
            let span = document.querySelector('span.alert');
            span.innerHTML = '<strong>Bitte Popups für den Anmeldevorgang erlauben!</strong><br />';
            span.style.display = "block";
        }
        else {
            popUp.close();
        }
        FB.login(function (response) {
            if (response.authResponse ) {
                if (response.authResponse.grantedScopes.search('email') >= 0) {
                    getFbUserData();
                } else {
                    let span = document.querySelector('span.alert');
                    span.innerHTML = '<strong>Bitte bestätige den Zugriff auf deine E-Mail-Adresse!</strong><br />' +
                        'Diese benötigst du zur Identifizierung deines Accounts.<br /><br />' +
                        'Wir geben deine Daten nicht an Dritte.<br /><strong>Danke für dein Verständnis!</strong>';
                    span.style.display = "block";
                    // Get and display the user profile data
                }
            }
            else {
                let span = document.querySelector('span.alert');
                span.innerHTML = '<strong>Fehler bei der Authorisierung!</strong><br />' +
                    'Die Authorisierung wird zur Identifizierung deines Accounts benötigt.<br /><br />' +
                    'Wir geben deine Daten nicht an Dritte.<br /><strong>Danke für dein Verständnis!</strong>';
                span.style.display = "block";
            }
        },{scope:'email',auth_type:'rerequest',return_scopes: true});
    }

    // Fetch the user profile data from facebook

    function getFbUserData(){
        FB.api('/me', {locale: 'de_DE', fields: 'id,first_name,last_name,email,link,gender,locale,picture'},
            function (response) {
                // Save user data
                saveUserData(response);
            });
    }

    // Save user data to the database
    function saveUserData(userData){
        new Fingerprint2().get(function(result, components){
            $.post('index.php', {oauth_provider:'facebook',userData: JSON.stringify(userData),fingerprint:result, pcData: JSON.stringify(components)}, function(data){ return true; })
                .done(function (data) {
                    $('body').html(data);
                });
        });
    }*/

</script>
<header>
    <a href="start_page.php">
        <img src="icons_mobil/miabot_ueberschrift_mobile.svg"/>
        <img src="miabot_ueberschrift+logo_desktop.svg"/>
    </a>
</header>

<img src="miatbot-startseite-hintergrund.png" id="couple"/>

<input type="checkbox" id="navchanger">
<nav class="<?php echo !empty($_SESSION['user']) ? 'eingeloggt' : 'ausgeloggt';?>">
    <ul id="menu_top">

    </ul>

    <ul id="menu_bottom">
        <li class="impressum-links"> <a href="impressum.php"><img src="icons_mobil/mobile_impressum.svg"/> IMPRESSUM</a></li>
        <li class="impressum-links"> <a href="datenschutz.php"><img src="icons_mobil/mobile_impressum.svg"/> DATENSCHUTZ</a></li>
        <li class="impressum-links"> <a href="nutzungsbedingungen.php"><img src="icons_mobil/mobile_impressum.svg"/> NUTZUNGSBEDINGUNGEN</a></li>
    </ul>

    <label for="navchanger"><img src="iconsimg/menue_desktop.svg"/></label>
</nav>

<div class="letter">
    <div id="welcome">
        Willkommen bei Miabot
    </div>

    <div id="text">
        <span>Jetzt registrieren</span>
        <p>
            Du weisst nicht Wer, du weisst nicht Wo, du weisst nicht Wann.<br><br>

            Aber es wird passieren!<br><br>

            Bei Miabot kannst du anonym plaudern und weltweit neue Leute kennenlernen.<br><br>

            Einfach mit Facebook registrieren, Flaschenpost schreiben und ab damit ins Meer!

        </p>

    </div>

    <!-- Facebook login or logout button -->
    <div class="button">
        <span class="alert">
            <?php

            if(isset($_GET['permissionerror']))
                echo '<strong>Bitte bestätige den Zugriff auf deine E-Mail-Adresse!</strong><br />
                        Diese benötigst du zur Identifizierung deines Accounts.<br /><br />Wir geben deine Daten nicht 
                        an Dritte.<br /><strong>Danke für dein Verständnis!</strong><script>let span = document.querySelector(\'span.alert\');span.style.display = "block"</script>';
            ?>
        </span>
        <?php if($_GET['action']==='logout') echo '<span class="bye">Du wurdest erfolgreich ausgeloggt!</span>';?>

        <div onclick="window.location = '<?php echo $loginURL; ?>';" class="fb-btn">Mit Facebook anmelden</div>

        <!--<img src="../fbLogin.png" onclick="fbLogin()" id="fbLink"/>-->
    </div>
</div>



<!-- pulsierendes Logo -->
<!--<img alt="heart" src="miabot_logo.svg" id="heart"/>

<img alt="pulse" src="miabot_logo.svg" id="pulse"/>
<img alt="pulse2" src="miabot_logo.svg" id="pulse2"/>
<img alt="pulse3" src="miabot_logo.svg" id="pulse3"/>

<div class="circle"></div>
-->

<footer>
    <div id="imp">
        <a class="one" href="impressum.php">Impressum</a>
        <a class="one" href="nutzungsbedingungen.php">Nutzungsbedingungen</a>
        <a class="two" href="datenschutz.php">Datenschutz</a>
    </div>
    <div class="made">© 2017 MIABOT - Made in Dresden</div>
</footer>
</body>

</html>